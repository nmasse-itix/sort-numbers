package main

var m [64]uint64 // bit mask

func init() {
	for i := 0; i < 64; i++ {
		m[i] = 1 << i
	}
}

type Bitmap []uint64

func NewBitmap(size uint) Bitmap {
	n := size / 64
	if n%64 > 0 {
		size += 1
	}
	return make(Bitmap, size)
}

func (b Bitmap) Get(pos int) bool {
	return b[pos/64]&m[pos%64] > 0
}

func (b Bitmap) Set(pos int) {
	b[pos/64] |= m[pos%64]
}
