package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBitmap(t *testing.T) {
	b := NewBitmap(999)
	for i := 0; i < 999; i++ {
		assert.Equal(t, false, b.Get(i))
	}
	b.Set(123)
	assert.Equal(t, true, b.Get(123))
	assert.Equal(t, false, b.Get(122))
	assert.Equal(t, false, b.Get(124))
}
