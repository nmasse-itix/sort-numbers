package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
)

func mysort(in io.Reader, out io.Writer) error {
	const max = 9999999
	var numbers = NewBitmap(max)

	// Read the file
	for {
		var s string
		_, err := fmt.Fscanln(in, &s)
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}

			return err
		}

		n, err := strconv.Atoi(s)
		if err != nil {
			return err
		}
		numbers.Set(n)
	}

	// Write the file
	for i := 0; i < max; i++ {
		if numbers.Get(i) {
			fmt.Fprintf(out, "%07d\n", i)
		}
	}

	return nil
}

func main() {
	err := mysort(os.Stdin, os.Stdout)
	if err != nil {
		panic(err)
	}
}
