package main

import (
	"sort"
	"testing"
)

const max int = 9999999
const n int = 1000000

var numbers [n]int

func init() {
	// two prime numbers (taken from http://compoasso.free.fr/primelistweb/page/prime/liste_online_en.php)
	p1 := 5000797
	p2 := 9999889

	for i := 0; i < n; i++ {
		numbers[i] = (i * p1) % p2
	}
}

func bitmapSort(numbers, sortedNumbers []int, n, max int) {
	var bitmap = NewBitmap((uint)(max))
	for i := 0; i < n; i++ {
		bitmap.Set(numbers[i])
	}

	c := 0
	for i := 0; i < max; i++ {
		if bitmap.Get(i) {
			sortedNumbers[c] = i
			c++
		}
	}
}

func stdSort(numbers []int, n, max int) {
	sort.Ints(numbers)
}

func TestBitmapSort(t *testing.T) {
	var sortedNumbers [n]int
	bitmapSort(numbers[:], sortedNumbers[:], n, max)
	for i := 1; i < n; i++ {
		if sortedNumbers[i-1] > sortedNumbers[i] {
			t.Fatal("wrong ordering")
		}
	}
}

func TestStdSort(t *testing.T) {
	sortedNumbers := numbers
	stdSort(sortedNumbers[:], n, max)
	for i := 1; i < n; i++ {
		if sortedNumbers[i-1] > sortedNumbers[i] {
			t.Fatal("wrong ordering")
		}
	}
}

func BenchmarkBitmapSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var sortedNumbers [n]int
		bitmapSort(numbers[:], sortedNumbers[:], n, max)
	}
}

func BenchmarkStdSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sortedNumbers := numbers
		stdSort(sortedNumbers[:], n, max)
	}
}
