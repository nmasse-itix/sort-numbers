package main

import (
	"io"
	"testing"
)

func BenchmarkGenRandomNumbers(b *testing.B) {
	for i := 0; i < b.N; i++ {
		genRandomNumbers(io.Discard)
	}
}
