package main

import (
	"fmt"
	"io"
	"os"
)

func genRandomNumbers(w io.Writer) {
	// two prime numbers (taken from http://compoasso.free.fr/primelistweb/page/prime/liste_online_en.php)
	p1 := 5000797
	p2 := 9999889

	for i := 0; i < 1000000; i++ {
		n := (i * p1) % p2
		fmt.Fprintf(w, "%07d\n", n)
	}
}

func main() {
	genRandomNumbers(os.Stdout)
}
