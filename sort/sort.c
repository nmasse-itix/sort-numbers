#include <stdio.h>
#include <stdlib.h>

int main (int argc, char** argv)
{
   printf ("int            is %lu bits wide\n", sizeof (int) * 8);
   printf ("long           is %lu bits wide\n", sizeof (long) * 8);
   printf ("unsigned int   is %lu bits wide\n", sizeof (unsigned int) * 8);
   printf ("unsigned long  is %lu bits wide\n", sizeof (unsigned long) * 8);

   if (argc < 2)
   {
      printf ("Please specify an input file. Exiting...\n");
      return 1;
   }

   printf ("Input file: %s\n", argv[1]);

   FILE *file;

   file = fopen(argv[1], "r"); // read mode

   if (file == NULL)
   {
      perror ("Error while opening the file.\n");
      return 1;
   }

   int bufferLength = 9;         // at most 7 digits + [newline] + [null]
   char buffer[bufferLength];

   while (fgets (buffer, bufferLength, file)) {
      int n = atoi (buffer);
      printf ("%d\n", n);

      // TODO...
   }

   fclose (file);
   return 0;

}

